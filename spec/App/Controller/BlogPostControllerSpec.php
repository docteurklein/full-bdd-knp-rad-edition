<?php

namespace spec\App\Controller;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Knp\RadBundle\Form\FormManager;
use App\Entity\BlogPost;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RouterInterface;
use Doctrine\Common\Persistence\ObjectRepository;

class BlogPostControllerSpec extends ObjectBehavior
{
    function let(ContainerInterface $container, FormInterface $form, FormView $formView, FormManager $formManager, BlogPost $blogPost, ManagerRegistry $doctrine, EntityManager $manager, Request $request, Session $session, AttributeBagInterface $bag, FlashBagInterface $flashBag, RouterInterface $router, ObjectRepository $repository)
    {
        $container->get('knp_rad.form.manager')->willReturn($formManager);
        $container->get('request')->willReturn($request);
        $container->get('router')->willReturn($router);
        $router->generate(Argument::cetera())->willReturn('/');
        $request->attributes = $bag;
        $session->getFlashBag()->willReturn($flashBag);
        $container->get('session')->willReturn($session);
        $container->has('doctrine')->willReturn(true);
        $container->get('doctrine')->willReturn($doctrine);
        $doctrine->getManager()->willReturn($manager);
        $container->get('app.entity.blog_post_repository')->willReturn($repository);
        $this->setContainer($container);

        $formManager->createBoundObjectForm(Argument::type('App\Entity\BlogPost'), Argument::cetera())->willReturn($form);
        $form->add('name', 'text')->willReturn($form);
        $form->createView()->willReturn($formView);
        $form->getData()->willReturn($blogPost);
    }

    function its_newAction_returns_a_form(FormView $formView, $form)
    {
        $form->isValid()->willReturn(false);

        $this->newAction()->shouldReturn([
            'form' => $formView,
        ]);
    }

    function its_newAction_should_save_object_when_valid(FormView $formView, $form, BlogPost $blogPost)
    {
        $form->isValid()->willReturn(true);

        $this->newAction()->shouldHaveType('Symfony\Component\HttpFoundation\RedirectResponse');
    }

    function its_indexAction_should_return_blogPost_list($repository, BlogPost $blogPost1, BlogPost $blogPost2)
    {
        $repository->findAll()->willReturn([$blogPost1, $blogPost2]);

        $this->indexAction()->shouldReturn([
            'blogPosts' => [$blogPost1, $blogPost2],
        ]);
    }
}
