<?php

namespace App\Controller;

use App\Controller\Controller;
use App\Entity\BlogPost;

class BlogPostController extends Controller
{
    public function newAction()
    {
        $form = $this->createBoundObjectForm(new BlogPost);

        if ($form->isValid()) {
            $this->persist($form->getData(), true);

            $this->addFlash('success');

            return $this->redirectToRoute('app_blogPost_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    public function indexAction()
    {
        $blogPosts = $this->get('app.entity.blog_post_repository')->findAll();

        return [
            'blogPosts' => $blogPosts
        ];
    }
}
